<?php namespace Clearweb\Postcode;

use Eloquent;

class Postcode extends Eloquent
{
    protected $table = '4pp_postcode';
    
    public static function getByPostcodeHouseNumber($postcode, $houseNumber)
    {
        if (preg_match('#^[0-9]{4} [A-Z][A-Z]$#', $postcode)) {
            str_replace(' ', '', $postcode);
        }
        
        $type = ($houseNumber % 2) ? 'odd' : 'even';
        
        return static::where('postcode', $postcode)
            ->where('minnumber', '<=', $houseNumber)
            ->where('maxnumber', '>=', $houseNumber)
            ->where(function($query) use ($type) {
                    $query->where('numbertype', '')
                        ->orWhere('numbertype', 'mixed')
                        ->orWhere('numbertype', $type)
                        ;
                })
            ->first()
            ;
    }
    
    public static function postcodeHouseNumberExists($postcode, $houseNumber)
    {
        $postcodeObject = static::getByPostcodeHouseNumber($postcode, $houseNumber);
        return ( ! empty($postcodeObject));
    }
}